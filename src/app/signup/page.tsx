"use client";
import React, { useState, ChangeEvent } from "react";
import Image from "next/image";
import Sidebar from "@/components/Sidebar";
import Carousel from "@/components/Carousel";
import ModalComponent from "@/components/ModalComponent";
import Link from 'next/link'

export default function Signup() {
  const [phoneNumber, setPhoneNumber] = useState("");

  const handleDelete = () => {
    setPhoneNumber("");
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setPhoneNumber(e.target.value);
  };

  return (
    <div className="container my-6 mx-auto px-8 max-w-[65rem]">
      <div className="grid grid-cols-12">
        <div className="col-span-12 hidden md:col-span-4 md:block p-4">
          <Sidebar />
        </div>
        <div className="col-span-12 md:col-span-8 h-max p-4 container py-8 rounded-xl border border-[#E8E8E8] mx-auto bg-white">
          <div className="my-12 px-[2rem]">
            <h1 className="font-sfpro mb-[20px] font-bold text-[30px] leading-[35.8px] tracking-[0.36px] text-center">
              Hey there! What&apos;s your number?
            </h1>
            <h2 className="font-sfpro font-light text-[#8A8A8E] text-[19px] leading-[24px] tracking-tight text-center">
              Yep, we ask for it straight away. This helps us weed out the
              weirdos and keep you and your data safe. We&apos;ll send you an
              SMS message to double check we&apos;ve got it right.
            </h2>
            <div className="mt-[40px] w-full flex justify-center items-center flex-col">
            <div className="relative w-[420px]">
              <div className="flex items-center border-b border-[#c6c6c8]">
                
                <Image
                  className="h-auto  mr-[10px]"
                  src="/assets/flag.svg"
              
                  alt="Australian Flag"
              width={28}
              priority
            />
                <span className="font-sfpro font-light text-[19px] mr-[45px] leading-[24px] tracking-tight text-gray-600">
                  +61
                </span>
                <input
                  type="tel"
                  className="py-[11px] pr-[16px] w-full font-light bg-transparent py-2 border-none focus:outline-none font-sfpro text-[19px] leading-[24px] tracking-tight text-gray-600"
                  placeholder="Mobile Number"
                  value={phoneNumber}
                  onChange={handleChange}
                />
                {phoneNumber && (
                  
                  <Image
                  className="text-gray-400 cursor-pointer ml-2"
              src="/assets/times.png"
              onClick={handleDelete}
              alt="times icon"
              width={17}
              priority
            />
                )}
              </div>
              <Link
              href="/verify"
         className={`mt-[28px] block text-center relative w-full text-[19px] py-3 px-4 md:px-20 focus:outline-none focus-visible:ring-2 focus-visible:ring-teal-600 focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900 rounded-[0.623rem] shadow ${
          phoneNumber.length === 10 ? 'bg-[#DC734C] text-white' : 'bg-[#E8E8E8] text-gray-600'
        }`} >
        Next
      </Link>
            </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  );
}


