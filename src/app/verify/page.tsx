"use client";
import React, { useState, ChangeEvent } from "react";
import Image from "next/image";
import Sidebar from "@/components/Sidebar";
import Carousel from "@/components/Carousel";
import ModalComponent from "@/components/ModalComponent";
import Link from 'next/link'

export default function Signup() {
  const [code, setCode] = useState("");

  const handleDelete = () => {
    setCode("");
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCode(e.target.value);
  };

  return (
    <div className="container my-6 mx-auto px-8 max-w-[65rem]">
      <div className="grid grid-cols-12">
        <div className="col-span-12 hidden md:col-span-4 md:block p-4">
          <Sidebar />
        </div>
        <div className="col-span-12 md:col-span-8 h-max p-4 container py-8 rounded-xl border border-[#E8E8E8] mx-auto bg-white">
          <div className="my-12 px-[2rem]">
            <h1 className="font-sfpro mb-[20px] font-bold text-[30px] leading-[35.8px] tracking-[0.36px] text-center">
            Verify your number
            </h1>
            <h2 className="font-sfpro font-light text-[#8A8A8E] text-[19px] leading-[24px] tracking-tight text-center">
            You should have a new message with verification code on 
+61 0499999999. Please type the code below.
            </h2>
            <div className="mt-[40px] w-full flex justify-center items-center flex-col">
            <div className="relative w-[420px]">
              <div className="flex items-center border-b border-[#c6c6c8]">
                <input
                  type="tel"
                  className="py-[11px] pr-[16px] w-full font-light bg-transparent py-2 border-none focus:outline-none font-sfpro text-[19px] leading-[24px] tracking-tight text-gray-600"
                  placeholder="Enter Code"
                  value={code}
                  onChange={handleChange}
                />
                {code && (
                  <Image
                  className="text-gray-400 cursor-pointer ml-2"
              src="/assets/times.png"
              onClick={handleDelete}
              alt="times icon"
              width={17}
              priority
            />
                )}
              </div>
              <p className="mt-[20px] font-sfpro font-light text-[#8A8A8E] text-[17px] leading-[22px] tracking-tight">
              Didn&apos;t receive a code? Resend in 20s
            </p>
            </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  );
}


