import Image from 'next/image';
import Sidebar from '@/components/Sidebar';
import Carousel from '@/components/Carousel';
import ModalComponent from '@/components/ModalComponent';

export default function Home() {
  return (
    <div className="container my-6 mx-auto px-8 max-w-[65rem]">
      <div className="grid grid-cols-12">
        <div className="col-span-12 hidden md:col-span-4 md:block p-4">
          <Sidebar />
        </div>
        <div className="col-span-12 md:col-span-8 p-4 container py-8 rounded-xl border border-[#E8E8E8] mx-auto bg-white">
          <Carousel />
          <div className="mt-12 px-6 lg:px-[6rem]">
            <h1 className="font-sfpro font-bold text-[30px] text-[#DB734C] leading-[32.05px] tracking-[0.36px] text-center">
              You were never meant <br /> to do this alone!
            </h1>
            <h2 className="mt-[1.375rem] md:mt-[1rem] font-sfpro font-light md:text-[21px] leading-[26px] tracking-[-0.41px] text-center">
              Let&apos;s make the most of your NDIS journey. Choose Kindship as your
              Plan Manager and we&apos;ll walk with you every step of the way.
            </h2>
            <div className="flex flex-col items-center mt-[2.875rem]">
              <ModalComponent />
              <div className="mt-[1.25rem] px-4 font-sfpro font-light text-[19px] text-[#8A8A8E] md:text-md leading-[25px] tracking-[-0.41px] text-center">
                Kindship Premium fees are fully <br /> funded by NDIS life
                choices.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}