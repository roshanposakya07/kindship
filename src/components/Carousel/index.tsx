'use client'
import React from "react";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider  from "react-slick";
import Card from "../Card";

const items = [
  { video: '/assets/vid1.mp4', name: "Tara Thompson", role: "Coach" },
  { video: '/assets/vid2.mp4', name: "Tara Thompson", role: "Coach" },
  { video: '/assets/vid3.mp4', name: "Tara Thompson", role: "Coach" },
  { video: '/assets/vid1.mp4', name: "Tara Thompson", role: "Coach" },
  { video: '/assets/vid2.mp4', name: "Tara Thompson", role: "Coach" },
];

export default function Carousel() {
  const settings = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "24px",
    slidesToShow: 3,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 3000,
    cssEase: "linear",
    speed: 500,
    dots: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <Slider {...settings}>
      {items.map((item, index) => (
        <Card key={index} data={item} />
      ))}
    </Slider>
  );
}