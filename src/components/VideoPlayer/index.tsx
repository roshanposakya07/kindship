import React from 'react';

interface VideoPlayerProps {
  data: {
    image: string;
  };
}

const VideoPlayer: React.FC<VideoPlayerProps> = ({ data }) => {
  return (
    <video className="w-full h-full object-cover p-2 rounded-3xl relative" autoPlay loop muted>
      <source src={data.image} type="video/mp4" />
      Your browser does not support the video tag.
    </video>
  );
};

export default VideoPlayer;