import React from "react";
import VideoPlayer from "../VideoPlayer";

interface CardProps {
  data: {
    video: string;
    name: string;
    role: string;
  };
}

export default function Card({ data }: CardProps) {
  return (
    <div className="focus:outline-none relative">
      <VideoPlayer data={{ image: data.video }} />

      <div className="caption absolute">
        <div className="gradient-bg absolute inset-0 w-full h-full bg-gradient-to-b from-transparent to-[#db734c] opacity-60 rounded-b-2xl" />

        <div className="caption-text">
          <p className="text-white text-sm sm:text-base md:text-lg font-bold">
            {data.name}
          </p>
          <p className="font-poppins font-light text-base lg:text-base leading-[22px] tracking-[-0.41px]">
            {data.role}
          </p>
        </div>
      </div>
    </div>
  );
}