import React from "react";
import Image from 'next/image'
import Link from 'next/link'

export default function Sidebar() {
  return (
    <div className="pr-8 my-[2rem] flex flex-col">
      <div className="mb-[8.118rem]">
      <Link href="/">
      <Image
          className="relative"
          src="/assets/logo.png"
          alt="Next.js Logo"
          width={129.91}
          height={29.89}
          priority
        />
        </Link>
        <h1 className="mt-[1.625rem] pr-8 leading-snug font-poppins font-normal text-[18px] leading-normal">
          Connect with parents who just get it!
        </h1>
      </div>
      <div className="mb-[4.063rem]">
      <Image
          className="relative mb-[1.312rem]"
          src="/assets/qr.png"
          alt="Next.js Logo"
          width={100}
          height={101.94}
          priority
        />
        <p className="mb-[0.75rem] leading-snug font-poppins font-semibold text-base lg:text-[15px] leading-[22px]">
          Scan the QR code <br /> and Explore our app
        </p>
        <p className="font-poppins font-normal text-[#7f7a78] text-[13px] leading-20 tracking-[-0.41px]">
          Available for IOS & Android
        </p>
      </div>
      <div className="mb-8">
        <p className="font-poppins font-medium text-[14px] leading-20 tracking-[-0.41px] mb-4">
          © 2023 Kindship
        </p>
        <ul className="mt-[1.625] gap-4">
          <li className="font-poppins mr-[1rem] font-normal text-[#7f7a78] text-[14px] leading-20 tracking-[-0.41px] inline-block">
            About App
          </li>
          <li className="font-poppins mr-[1rem] font-normal text-[#7f7a78] text-[14px] leading-20 tracking-[-0.41px] inline-block">
            Privacy Policy
          </li>
          <li className="font-poppins font-normal text-[#7f7a78] text-[14px] leading-20 tracking-[-0.41px] inline-block">
            Terms
          </li>
        </ul>
      </div>
    </div>
  );
}
