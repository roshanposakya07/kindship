'use client'
import ReactModal from 'react-modal';
import { useState } from 'react';
import Image from 'next/image';
import Link from 'next/link'

const ModalComponent = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [step, setStep] = useState(1);

  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
    setStep(1); // Reset the step when the modal is closed
  };

  const handleNext = () => {
    setStep((prevStep) => prevStep + 1);
  };

  const handleSignUp = () => {
    console.log('Sign Up button clicked');
  };

  const renderModalContent = () => {
    switch (step) {
      case 1:
        return (
          <div>
            <Image
              className="block mx-auto mb-4"
              src="/assets/icon.png"
              alt="Next.js Logo"
              width={64}
              height={64}
              priority
            />
            <div className="mb-[22px] font-sfpro font-semibold text-[30px] leading-[35.8px] tracking-[0.36px] text-center">
              Premium Support
            </div>
            <div className="mb-[34px] font-sfpro font-normal text-[21px] text-[#8A8A8E] leading-[25.06px] text-center">
              {`Our support team (AKA "coaches") are parents who live and breathe disability. We'll help you make the most of the NDIS!`}
            </div>
            <button
              onClick={handleNext}
              className="relative w-[22rem] text-lg font-semibold font-[19px] bg-[#DC734C] text-white py-3 px-4 md:px-20 focus:outline-none focus-visible:ring-2 focus-visible:ring-teal-600 focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900 rounded-[0.623rem] shadow"
            >
              Next
            </button>
          </div>
        );
      case 2:
        return (
          <div>
            <Image
              className="block mx-auto mb-4"
              src="/assets/payment-icon.png"
              alt="Next.js Logo"
              width={64}
              height={64}
              priority
            />
            <div className="mb-[22px] font-sfpro font-semibold text-[30px] leading-[35.8px] tracking-[0.36px] text-center">
              Fast Payments
            </div>
            <div className="mb-[34px] font-sfpro font-normal text-[21px] text-[#8A8A8E] leading-[25.06px] text-center">
              {`We're all about fast painless plan management because we know you've got more important things to do.`}
            </div>
            <button
              onClick={handleNext}
              className="relative w-[22rem] text-lg font-semibold font-[19px] bg-[#DC734C] text-white py-3 px-4 md:px-20 focus:outline-none focus-visible:ring-2 focus-visible:ring-teal-600 focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900 rounded-[0.623rem] shadow"
            >
              Next
            </button>
          </div>
        );
      case 3:
        return (
          <div>
            <Image
              className="block mx-auto mb-4"
              src="/assets/resources-icon.png"
              alt="Next.js Logo"
              width={64}
              height={64}
              priority
            />
            <div className="mb-[22px] font-sfpro font-semibold text-[30px] leading-[35.8px] tracking-[0.36px] text-center">
              Free Resources
            </div>
            <div className="mb-[34px] font-sfpro font-normal text-[21px] text-[#8A8A8E] leading-[25.06px] text-center">
              {`Become the best advocate you can be! We've crowdsourced the very best advice and support tools from families kicking NDIS goals.`}
            </div>
            <Link 
            href="/signup"
            className="relative block text-center w-[22rem] text-lg font-semibold font-[19px] bg-[#DC734C] text-white py-3 px-4 md:px-20 focus:outline-none focus-visible:ring-2 focus-visible:ring-teal-600 focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900 rounded-[0.623rem] shadow"
            >
              Sign up
            </Link>
          </div>
        );
      default:
        return null;
    }
  };

  return (
    <div>
      <button
        onClick={openModal}
        className="relative w-[22rem] text-lg font-semibold font-[19px] bg-[#DC734C] text-white py-3 px-4 md:px-20 focus:outline-none focus-visible:ring-2 focus-visible:ring-teal-600 focus-visible:ring-offset-2 focus-visible:ring-offset-gray-900 rounded-[0.623rem] shadow"
      >
        Learn more
      </button>
      <ReactModal
        isOpen={isOpen}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
      >
        <div className="text-base flex justify-end block font-base font-bold capitalize">
          <svg
            onClick={closeModal}
            className="cursor-pointer absolute top-[10px] right-[10px] mr-2 rotate-[130deg] svg-circleplus h-[3rem] stroke-[#000]"
            viewBox="0 0 100 100"
          >
            <line x1="32.5" y1="50" x2="67.5" y2="50" strokeWidth="5"></line>
            <line x1="50" y1="32.5" x2="50" y2="67.5" strokeWidth="5"></line>
          </svg>
        </div>
        {renderModalContent()}
      </ReactModal>
    </div>
  );
};

export default ModalComponent;