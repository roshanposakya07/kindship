"use strict";
(() => {
var exports = {};
exports.id = 155;
exports.ids = [155];
exports.modules = {

/***/ 2037:
/***/ ((module) => {

module.exports = require("os");

/***/ }),

/***/ 6940:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  headerHooks: () => (/* binding */ headerHooks),
  originalPathname: () => (/* binding */ originalPathname),
  requestAsyncStorage: () => (/* binding */ requestAsyncStorage),
  routeModule: () => (/* binding */ routeModule),
  serverHooks: () => (/* binding */ serverHooks),
  staticGenerationAsyncStorage: () => (/* binding */ staticGenerationAsyncStorage),
  staticGenerationBailout: () => (/* binding */ staticGenerationBailout)
});

// NAMESPACE OBJECT: ./node_modules/next/dist/build/webpack/loaders/next-metadata-route-loader.js?page=%2Ffavicon.ico%2Froute&isDynamic=0!./src/app/favicon.ico?__next_metadata_route__
var favicon_next_metadata_route_namespaceObject = {};
__webpack_require__.r(favicon_next_metadata_route_namespaceObject);
__webpack_require__.d(favicon_next_metadata_route_namespaceObject, {
  GET: () => (GET),
  dynamic: () => (dynamic)
});

// EXTERNAL MODULE: ./node_modules/next/dist/server/node-polyfill-headers.js
var node_polyfill_headers = __webpack_require__(6519);
// EXTERNAL MODULE: ./node_modules/next/dist/server/future/route-modules/app-route/module.js
var app_route_module = __webpack_require__(3488);
var module_default = /*#__PURE__*/__webpack_require__.n(app_route_module);
;// CONCATENATED MODULE: external "next/server"
const server_namespaceObject = require("next/server");
;// CONCATENATED MODULE: ./node_modules/next/dist/build/webpack/loaders/next-metadata-route-loader.js?page=%2Ffavicon.ico%2Froute&isDynamic=0!./src/app/favicon.ico?__next_metadata_route__


const contentType = "image/x-icon"
const buffer = Buffer.from("AAABAAEAHhwAAAEAIAC4DQAAFgAAACgAAAAeAAAAOAAAAAEAIAAAAAAAIA0AABMLAAATCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATHTbQEx024BNc9yvTHTcv0x03c9MdNy/TXPcr0xz3JBNct1wUHDfIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUHDfIE1z2qBNc93fTHPc/0xz3P9Mc9z/S3Pc30x03c9Lc9zfTXPd70xz3P9Mc9z/THPc/0x03c9MdNuAUHDfEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMdNuATHPc/0xz3P9Nc93fTXTdj0x020AAAAAAAAAAAAAAAAAAAAAAAAAAAFBw3yBKddpgTXTdj01z3e9Mc9z/TXPd701z3FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUHDfIExz3c9Mc9z/TXPd301y3GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFCA3xBMct1/TXPd70xz3P9Mc9yQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQcN8gTXPd30xz3P9MdN3PUHjfIAAAAAAAAAAAUHDfIE1y3XBMdNy/TXPd70xz3P9Mc9z/S3Pc30x03L9Mc9yQTHTbQAAAAAAAAAAAUHjfIE1z3d9Mc9z/S3PaoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFBw3xBMc93PTHPc/01z3J8AAAAAAAAAAAAAAABMc9yQTHPc/0xz3P9Mc9z/THPc/0xz3P9Mc9z/THPc/0xz3P9Mc9z/THPc/0tz2qBQcN8QAAAAAFB43yBMc9z/THPc/0xy24AAAAAAAAAAAAAAAAAAAAAAAAAAAExz3JBMc9z/THPcvwAAAAAAAAAAAAAAAEtz2qBMc9z/THPc/01z3d9NctxgSnXaMAAAAAAAAAAAUHDfIE1y3XBNc93fTHPc/0xz3P9MdN3PUHDfEAAAAABNc9yfTHPc/0xz3P9NctxgAAAAAAAAAAAAAAAATHTbQExz3P9Nc93fUIDfEAAAAAAAAAAATXLdcExz3P9Mc9z/THLdf1CA3xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQgN8QTXLdj0xz3P9Mc9z/TXPcnwAAAABQeN8gTXPd30xz3P9Nc93fAAAAAAAAAAAAAAAATHPcv0xz3P9Nc9xQAAAAAAAAAABMdNtATHPc/0xz3O9MdNtAAAAAAAAAAABNctxgTHPckE1z3Z9MdNuASnXaYFBw3zAAAAAAAAAAAEx020BNc93vTHPc/01y3GAAAAAATXLcYExz3P9Mc9z/TXLcYAAAAABQcN8wTHPc/0xz3L8AAAAAAAAAAAAAAABMc93PTHPc/0p12jAAAAAAUHDfIE1z3d9Mc9z/THPc/01z3e9MdN3PTXPd70xz3P9Lc9qgAAAAAAAAAABOc99vTHPc/01z3d8AAAAAAAAAAE1z3d9Mc9z/THTdzwAAAABMdNuATHPc/0t023AAAAAAAAAAAE1y3GBMc9z/THLbgAAAAABQcN8QTXPd301z3d9NctxgUHjfIAAAAAAAAAAAAAAAAEx03X9Mc9z/THLbgAAAAAAAAAAATXPd30xz3P9MdNtAAAAAAE1y3GBMc9z/THPc/1Bw3yBMc9y/THPc/1Bw3yAAAAAAAAAAAExz3L9Mc9zvUIDfEAAAAABMdNuATHPc71B43yAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABNc9yfTXPd3wAAAAAAAAAATXPcn0xz3P9MdNuAAAAAAAAAAABNc93fTHPc/01z3FBLc9zfTHPc7wAAAAAAAAAAUHDfEExz3P9Nc92fAAAAAAAAAABNc93vTXPdnwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMdNtATHPc/0x020AAAAAATHTbQExz3P9Mc9y/AAAAAAAAAABNc9yvTHPc/0xz3JBMc9z/THTdzwAAAAAAAAAAUHXfMExz3P9MdNuAAAAAAFBw3yBMc9z/THTbgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQcN8gTHPc/0113GAAAAAAUHDfEExz3P9Mc9y/AAAAAAAAAABMdNuATHPc/01z3Z9Mc9z/THPcvwAAAAAAAAAATHTbQExz3P9MdNuAAAAAAFBw3yBMc9z/THPckAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABQcN8gTHPc/0x024AAAAAAAAAAAExz3P9Mc9y/AAAAAAAAAABNct1wTHPc/01z3Z9Nc93vTHPc7wAAAAAAAAAAUHDfIExz3P9Mc9yQAAAAAAAAAABNc93fTXPd3wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMdNtATHPc/0113GAAAAAAUHDfIExz3P9Nc92fAAAAAAAAAABMdNuATHPc/01z3Z9Mc9y/THPc/0p12jAAAAAAAAAAAE1z3d9MdN3PAAAAAAAAAABMdN1/THPc/01y3GAAAAAAAAAAAAAAAAAAAAAAAAAAAFBw3yBNc93fTHPc71CA3xAAAAAATXPcUExz3P9MctuAAAAAAAAAAABMc9y/THPc/0t023BNctxgTHPc/01z3J8AAAAAAAAAAE1z3J9Mc9z/THTbQAAAAAAAAAAATHTdz01z3e9MdNtAUHDfEAAAAABQcN8wTXLcYE1z3d9Mc9z/S3LbcAAAAAAAAAAATHPcv0xz3P9MdNtAAAAAAFBw3xBNc93vTHPc/0x020BQeN8gTHPc/0xz3P9QcN8gAAAAAFB43yBMc9z/THXcrwAAAAAAAAAAUHjfIEx03c9Mc9z/THPc/0xz3P9Mc9z/THPc/01z3e9Mct1/AAAAAAAAAABMdNtATHPc/0xz3L8AAAAAAAAAAEx024BMc9z/THPcvwAAAAAAAAAATXPcn0xz3P9MdN3PAAAAAAAAAABNc9yfTHPc/01y3GAAAAAAAAAAAAAAAABNct1wTXLdn01y3Z9MdNuATHTbQAAAAAAAAAAAAAAAAFBw3xBNc93fTHPc/0x020AAAAAAUHDfIE1z3e9Mc9z/TXLcYAAAAAAAAAAAUHjfIE1z3d9Mc9z/S3TbcAAAAABQgN8QTXPd301z3d9QcN8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUHDfIE1z3d9Mc9z/TnPfXwAAAAAAAAAATHPcv0xz3P9Mc9y/AAAAAAAAAAAAAAAAAAAAAE1y3GBMc9z/THPc/1Bw3yAAAAAAUHjfIE1z3d9Nc93vTHTbgFBw3xAAAAAAAAAAAAAAAAAAAAAAAAAAAFBw3yBMdNuATHPc/01z3d9KddowAAAAAAAAAABMc9yQTHPc/01z3d9QeN8gAAAAAAAAAAAAAAAAAAAAAAAAAABMdN1/THPc/01z3d9QcN8gAAAAAFB43yBMc9y/THPc/0xz3P9MdNy/TXLdcEp12mBNct1wTXPcr0xz3P9Mc9z/TXPd31B43yAAAAAAAAAAAExz3JBMc9z/THPc70p12jAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATXPcr0xz3P9Nc93fUHDfMAAAAAAAAAAATnPfb01z3e9Mc9z/THPc/0xz3P9Mc9z/TXPd70xz3K9NctxgAAAAAAAAAABQcN8gTXPd30xz3P9Nc93fSnXaMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUIDfEEx03c9Mc9z/TXPd70x024BQcN8QAAAAAAAAAABQcN8gTHTbQEx020BQgN8QAAAAAAAAAAAAAAAAAAAAAEx024BMc9z/THPc/01z3d9QeN8gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFCA3xBLc9yfTHPc/0xz3P9Nc93vTHPckEx020AAAAAAAAAAAAAAAAAAAAAAAAAAAFBw3zBNc9qgTXPd30xz3P9Mc9z/TXTbjwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATnPfb01z3e9Mc9z/THPc/0xz3P9Nc93vTHTdz0x03L9MdNy/TXPd70xz3P9Mc9z/THPc/01z3d9NctxgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFCA3xBNct1wTHPcr0tz3N9Mc9z/THPc/0xz3P9Mc9z/S3Pc30xz3L9NdN2PTXPcUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/wA/8/gAB/PwPgPzwf/B84MAMPMOAAhzHAwEMhg/BDIwwMIQcQBjEGIOMQBiPzGAxn8RgMR/EYDEfxmAxn8RgGY+EYBjCDEAIwBjEjHBwhIQ/4YzCD4MM4QAGHPDAGDzwMPB8+A+B/P4AA/z/AA/8", 'base64'
  )

function GET() {
  return new server_namespaceObject.NextResponse(buffer, {
    headers: {
      'Content-Type': contentType,
      'Cache-Control': "public, max-age=0, must-revalidate",
    },
  })
}

const dynamic = 'force-static'

;// CONCATENATED MODULE: ./node_modules/next/dist/build/webpack/loaders/next-app-loader.js?page=%2Ffavicon.ico%2Froute&name=app%2Ffavicon.ico%2Froute&pagePath=private-next-app-dir%2Ffavicon.ico&appDir=%2FUsers%2Fcodekarkhana%2FDesktop%2Ftask%2Fkindshipweb%2Fsrc%2Fapp&appPaths=%2Ffavicon.ico&pageExtensions=tsx&pageExtensions=ts&pageExtensions=jsx&pageExtensions=js&basePath=&assetPrefix=&nextConfigOutput=&preferredRegion=&middlewareConfig=e30%3D!

    

    

    

    const options = {"definition":{"kind":"APP_ROUTE","page":"/favicon.ico/route","pathname":"/favicon.ico","filename":"favicon","bundlePath":"app/favicon.ico/route"},"resolvedPagePath":"next-metadata-route-loader?page=%2Ffavicon.ico%2Froute&isDynamic=0!/Users/codekarkhana/Desktop/task/kindshipweb/src/app/favicon.ico?__next_metadata_route__","nextConfigOutput":""}
    const routeModule = new (module_default())({
      ...options,
      userland: favicon_next_metadata_route_namespaceObject,
    })

    // Pull out the exports that we need to expose from the module. This should
    // be eliminated when we've moved the other routes to the new format. These
    // are used to hook into the route.
    const {
      requestAsyncStorage,
      staticGenerationAsyncStorage,
      serverHooks,
      headerHooks,
      staticGenerationBailout
    } = routeModule

    const originalPathname = "/favicon.ico/route"

    

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [697,451], () => (__webpack_exec__(6940)));
module.exports = __webpack_exports__;

})();